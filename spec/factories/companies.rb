FactoryBot.define do
  factory :company do
    name { 'My Company' }
  end
  factory :second_company, class: Company do
    name { 'New Company' }
  end
  factory :multiple_companies, class: Company do
    sequence(:name) { |n| "Company #{n}" }
  end
end
