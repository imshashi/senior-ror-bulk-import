FactoryBot.define do
  factory :employee do
    name { 'Jon Snow' }
    email { 'test@test.com' }
    company
  end

  factory :second_employee, class: Employee do
    name { 'Joe Doe' }
    email { 'test@email.com' }
    company
  end

  factory :employee_same_email, class: Employee do
    name { 'David Wu' }
    email { 'test@test.com' }
    company
  end
end
