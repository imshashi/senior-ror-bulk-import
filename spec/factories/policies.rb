FactoryBot.define do
  factory :policy do
    name { 'Sick Leave' }
    company
  end

  factory :second_policy, class: 'Policy' do
    name { 'Annual Leave' }
    company
  end

  factory :policy_same_name, class: 'Policy' do
    name { 'Sick Leave' }
    company
  end
end
