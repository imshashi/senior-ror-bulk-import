Rails.application.routes.draw do
  root 'home#index'
  resources :policies
  resources :companies
  resources :employees

  get 'settings/new-bulk-import', to: 'settings#new_bulk_import'
  post 'settings/create-bulk-import', to: 'settings#create_bulk_import'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
